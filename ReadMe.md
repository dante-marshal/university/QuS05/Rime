# About
RIME (Rime Intelligent Melody Emitter) is an Artificial Neural Network which can be trained on a set of Melodies to generate and complete other similar melodies.