class RimeEmitter:
	pathToModel: str;
	pathToInput: str;
	pathToOutput: str;
	def __init__(self, pathToModel: str, pathToInput: str, pathToOutput: str):
		self.pathToModel = pathToModel;
		self.pathToInput = pathToInput;
		self.pathToOutput = pathToOutput;
	def Run(self) -> None:
		from Model.RimeModel import RimeModel;
		from Model.Dataset.RimeDataset import RimeDataset;
		# Load the Model
		print('Loading the Model ...');
		self.theModel = RimeModel(self.pathToModel, self.pathToInput);
		# Create the Dataset Handler
		theDataset = RimeDataset(self.pathToInput, self.theModel.params.dataLengthRange, 1);
		# Predict
		print('Emitting ...');
		emission = self.theModel.Emit(theDataset);
		# Convert the emission to Midi
		from Model.Dataset.RimeMidi import RimeMidi;
		emissionInput = RimeMidi(emission[0]);
		emissionOutput = RimeMidi(emission[1]);
		# Save the Emission files
		print('Saving the Output ...');
		from os.path import isdir as PIsDir;
		if not PIsDir(self.pathToOutput):
			from os import makedirs as MakeDirs;
			MakeDirs(self.pathToOutput);
		from os.path import join as PJoin;
		emissionInput.SaveMidi(PJoin(self.pathToOutput, "Input.midi"));
		emissionInput.SaveMatrix(PJoin(self.pathToOutput, "Input.png"));
		emissionOutput.SaveMidi(PJoin(self.pathToOutput, "Output.midi"));
		emissionOutput.SaveMatrix(PJoin(self.pathToOutput, "Output.png"));
		from numpy import concatenate as NpConcatenate;
		emission = NpConcatenate(emission, axis = 0);
		emission = RimeMidi(emission);
		emission.SaveMidi(PJoin(self.pathToOutput, "Emission.midi"));
		emission.SaveMatrix(PJoin(self.pathToOutput, "Emission.png"));
