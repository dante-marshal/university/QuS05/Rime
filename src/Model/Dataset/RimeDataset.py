import random as Random;
from typing import Generator, List, Tuple, Union

from Model.Dataset.RimeMidi import RimeMidi;
from numpy import ndarray as NumpyArray, concatenate as NpConcat, zeros as NpZeros, ones as NpOnes, array as NpArray;

def GetFilesRecursive(dir: str, ext: str) -> List[str]:
	ret: List[str] = [];
	from os import listdir;
	fs: List[str] = listdir(dir);
	from os import path;
	for f in fs:
		fPath = path.join(dir, f);
		if path.isfile(fPath) and f.endswith(ext):
			ret.append(fPath);
		elif path.isdir(fPath):
			ret += GetFilesRecursive(fPath, ext);
	return ret;

class RimeBatchInstance:
	theMatrix: NumpyArray; # 2D
	nextStartIndex:int;
	featureCount: int;
	finelPaddedLength: int;
	dataLengthRange: Tuple[int, int];
	lastInputRange: Tuple[int, int];
	lastOutputRange: Tuple[int, int];
	def __init__(self, pathToInput: str, dataLengthRange: Tuple[int, int], finelPaddedLength: int):
		# finalPaddedLength cannot be smaller than dataMaxLength + 4, where 4 is the length of the start + end markers
		if finelPaddedLength < (dataLengthRange[1] + 4):
			raise Exception(F"Padded Length is too small; Expected > {dataLengthRange[1] + 3}, Found : {finelPaddedLength}");
		self.dataLengthRange = dataLengthRange;
		self.finelPaddedLength = finelPaddedLength;
		self.theMatrix = RimeMidi(pathToInput).Vectorize();
		self.featureCount = self.theMatrix.shape[1];
		self.nextStartIndex = 0;
		self.lastInputRange = (-1, -1);
		self.lastOutputRange = (-1, -1);
	def NextInterval(self, useRandomStart: bool = False) -> bool:
		# Calculate input start index
		inputStartIndex: int = 0;
		if useRandomStart:
			inputStartIndex: int = Random.randint(0, self.theMatrix.shape[0] - 2 * self.dataLengthRange[1]);
		else:
			randomOverlapLength: int = Random.randint(0, self.dataLengthRange[0]);
			if self.nextStartIndex > randomOverlapLength:
				inputStartIndex: int = self.nextStartIndex - randomOverlapLength;
		# Calculate input and output lengths based on how much data we have remaining
		remainingDataLength: int = self.theMatrix.shape[0] - inputStartIndex;
		lengthOfInput: int;
		lengthOfOutput: int;
		# If we have enough data, use the full length
		if remainingDataLength > (2 * self.dataLengthRange[1]):
			lengthOfInput: int = Random.randint(self.dataLengthRange[0], self.dataLengthRange[1]);
			lengthOfOutput: int = Random.randint(self.dataLengthRange[0], self.dataLengthRange[1]);
		# If we don't have enough data, use the remaining possible data
		elif remainingDataLength > (2 * self.dataLengthRange[0]):
			maxLength = self.dataLengthRange[1];
			# Choose a random index to split the data from
			lengthOfInput: int = Random.randint(self.dataLengthRange[0], remainingDataLength - self.dataLengthRange[0]);
			lengthOfInput = min(lengthOfInput, maxLength);
			lengthOfOutput: int = remainingDataLength - lengthOfInput;
			if lengthOfInput > maxLength:
				lengthOfInput = maxLength;
			if lengthOfOutput > maxLength:
				lengthOfOutput = maxLength;
		else:
			return False;
		# Calculate Indices based on lengths
		outputStartIndex: int = inputStartIndex + lengthOfInput;
		# Set indices to load from on next getInput() and getOutput()
		self.nextStartIndex = outputStartIndex + lengthOfOutput;
		self.lastInputRange = (inputStartIndex, inputStartIndex + lengthOfInput);
		self.lastOutputRange = (outputStartIndex, outputStartIndex + lengthOfOutput);
		return True;
	def WithStartAndEnd(self, data: NumpyArray) -> NumpyArray:
		return NpConcat([
			NpZeros((1, self.featureCount)),
			NpOnes((1, self.featureCount)),
			data,
			NpOnes((1, self.featureCount)),
			NpZeros((1, self.featureCount))
		]);
	def getInput(self) -> NumpyArray:
		# self.lastInputRange[1] - self.lastInputRange[0] <= self.finalPaddedLength - 4
		ret: NumpyArray = self.theMatrix[self.lastInputRange[0]:self.lastInputRange[1], :];
		ret: NumpyArray = self.WithStartAndEnd(ret);
		if ret.shape[0] < self.finelPaddedLength:
			# # Add Zero-Padding @ Start
			# pad = NpZeros((self.finelPaddedLength - ret.shape[0], self.featureCount));
			# ret = NpConcat([ pad, ret ]);
			# Add Zero-Padding @ End
			pad = NpZeros((self.finelPaddedLength - ret.shape[0], self.featureCount));
			ret = NpConcat([ ret, pad ]);
		return ret;
	def getOutput(self) -> NumpyArray:
		# self.lastOutputRange[1] - self.lastOutputRange[0] <= self.finalPaddedLength - 4
		ret: NumpyArray = self.theMatrix[self.lastOutputRange[0]:self.lastOutputRange[1], :];
		ret: NumpyArray = self.WithStartAndEnd(ret);
		if ret.shape[0] < self.finelPaddedLength:
			# # Add Zero-Padding @ Start
			# pad = NpZeros((self.finelPaddedLength - ret.shape[0], self.featureCount));
			# ret = NpConcat([ pad, ret ]);
			# Add Zero-Padding @ End
			pad = NpZeros((self.finelPaddedLength - ret.shape[0], self.featureCount));
			ret = NpConcat([ ret, pad ]);
		return ret;
	
class EmptyRimeBatchInstance:
	def NextInterval(self, useRandomStart: bool = False) -> bool:
		return False;

# Manager class to split data into batches with random variable lengths
class RimeDataset:
	# Initialization
	finelPaddedLength: int;
	inputFilePaths: List[str];
	# dataLengthRange = (dataMinLength, dataMaxLength)
	dataLengthRange: Tuple[int, int];
	loadedData: List[RimeBatchInstance];
	def __init__(self, pathToInput: str, dataLengthRange: Tuple[int, int], maxBatchSize: int, maxFileCount: Union[int, None] = None, shuffleSeed: float = None):
		self.loadedData = [];
		self.dataLengthRange = dataLengthRange;
		# finalPaddedLength = dataMaxLength + 4, where 4 is the length of the start + end markers
		self.finelPaddedLength = dataLengthRange[1] + 4;
		# List and Shuffle inputs
		self.inputFilePaths = GetFilesRecursive(pathToInput, '.midi') + GetFilesRecursive(pathToInput, '.mid');
		Random.shuffle(self.inputFilePaths, Random.Random(shuffleSeed).random);
		# Cut some data out if maxFileCount is specified
		if (maxFileCount is not None) and (maxFileCount < len(self.inputFilePaths)):
			self.inputFilePaths = self.inputFilePaths[0:maxFileCount]
		# Initialize loadedData variable with empty batches, so that the first call to NextBatch() will load data
		for _ in range(maxBatchSize):
			self.loadedData.append(EmptyRimeBatchInstance())
	lastInputFile: int = -1;
	def NextBatch(self, randomBatch: bool = False) -> bool:
		dataLen: int = len(self.loadedData);
		toRemove: List[int] = [];
		# Load new data (next intervals) in every batch
		for i in range(dataLen):
			while not self.loadedData[i].NextInterval(randomBatch):
				# If the batch is finished, load a new one
				self.lastInputFile += 1;
				if self.lastInputFile < len(self.inputFilePaths):
					print(F"\tOpening Data {self.lastInputFile: 4d} / {len(self.inputFilePaths)} @ Index {i: 4d} : '{self.inputFilePaths[self.lastInputFile]}'");
					self.loadedData[i] = RimeBatchInstance(self.inputFilePaths[self.lastInputFile], self.dataLengthRange, self.finelPaddedLength);
				else:
					# If there is no more data (no new batch to load), remove the batch
					toRemove.append(i);
					break;
		# Remove batches that we have no more data for
		toRemove.sort(reverse=True);
		for i in toRemove:
			print(F"\n\tRemoving last Data @ {i}");
			self.loadedData.pop(i);
		return len(self.loadedData) > 0;
	def getBatchInput(self) -> NumpyArray:
		intervals: List[NumpyArray] = map(lambda d: d.getInput(), self.loadedData);
		return NpArray(list(intervals));
	def getBatchInputWithLSTM(self, numLatentNotes: int) -> NumpyArray:
		ret = self.getBatchInput();
		return [ret, NpZeros((ret.shape[0], numLatentNotes))];
	# Gets the current batch output of loadedData
	def getBatchOutput(self) -> NumpyArray:
		intervals: List[NumpyArray] = map(lambda d: d.getOutput(), self.loadedData);
		return NpArray(list(intervals));
	# Generator function for the dataset to feed into the model
	def Generate(self, maxEpochCount: int, numLatentNotes: int) -> Generator[Tuple[List[NumpyArray], NumpyArray], None, None]:
		epochNumber: int = 0;
		while epochNumber < maxEpochCount and self.NextBatch():
			dIn = self.getBatchInputWithLSTM(numLatentNotes);
			dOut = self.getBatchOutput();
			epochNumber += 1;
			yield (dIn, dOut);
