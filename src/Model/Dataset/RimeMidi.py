from numpy import ndarray as NumpyArray;
from typing import Iterable, List, Dict, Tuple, Union;
class RimeMidi:
	BEAT_RESOLUTION: int = 4;
	# RimeMidi
	def __init__(self, src: Union[str, NumpyArray]):
		self.states = {};
		if isinstance(src, str):
			from os.path import isfile;
			if not isfile(src):
				raise FileNotFoundError(src);
			if src.endswith('.midi') or src.endswith('.mid'):
				self.LoadMidi(src);
			elif src.endswith('.png'):
				self.LoadMatrix(src);
			else:
				raise ValueError(F'Unrecognized file type : {src}')
		elif isinstance(src, NumpyArray):
			self.DeVectorize(src);
		else:
			raise TypeError(F"Invalid Type '{type(src)}' for RimeMidi constructor input");
	# RimeMidi : States
	states: Dict[float, List[Tuple[int, float]]];
	# IO : Midi
	def LoadMidi(self, src: str) -> None:
		from mido import MidiFile;
		src: MidiFile = MidiFile(src);
		tCount: int = len(src.tracks);
		from mido.messages.messages import Message as MidiMessage;
		from mido.midifiles.tracks import _to_abstime as MidiTimeToAbsolute;
		# Currently only supports a single track named "Default", Nothing else !
		ms: Iterable[MidiMessage] = [];
		for i in range(tCount):
			if src.tracks[i].name == "Default":
				ms = MidiTimeToAbsolute(src.tracks[i]);
				break;
		for m in ms:
			if m.type.startswith('note'):
				t: float = m.time / src.ticks_per_beat; # Beat Number
				s: Tuple[int, float] = (m.note, (m.velocity / 127.0) if m.type == 'note_on' else 0.0);
				if t not in self.states:
					self.states[t] = [];
				self.states[t].append(s);
	def SaveMidi(self, dst: str) -> None:
		from mido import MidiFile, MidiTrack;
		midi: MidiFile = MidiFile();
		from mido.messages.messages import Message as MidiMessage;
		ms: List[MidiMessage] = [];
		for t in self.states:
			ns = self.states[t];
			mt = round(t * midi.ticks_per_beat);
			for n in ns:
				if n[1] > 0.0:
					ms.append(MidiMessage(
						'note_on', time=mt,
						note=n[0], velocity=round(n[1] * 127)
					));
				else:
					ms.append(MidiMessage(
						'note_off', time=mt,
						note=n[0], velocity=0
					));
		from mido.midifiles.tracks import _to_reltime as MidiTimeToRelative;
		t: MidiTrack = midi.add_track('Default');
		t.extend(MidiTimeToRelative(ms));
		midi.save(dst);
	# IO : Matrix
	def LoadMatrix(self, src: str) -> None:
		from os.path import isfile;
		if not isfile(src):
			raise FileNotFoundError(F"File '{src}' not found !");
		from PIL.Image import open as ImageFromFile;
		with ImageFromFile(src) as img:
			import numpy;
			self.DeVectorize(numpy.array(img));
	def SaveMatrix(self, dst: str) -> None:
		from PIL.Image import Image, fromarray as ImageFromMatrix;
		img: Image = ImageFromMatrix(self.Vectorize(), 'L');
		img.save(dst);
	# Vectorization
	def DeVectorize(self, src: NumpyArray) -> None:
		import numpy as np;
		if src.shape[1] != 128:
			raise TypeError(F'Invalid matrix width shape; Expected (:, 128), Found (:, {src.shape[1]})');
		rowCount: int = src.shape[0] - 1;
		lastState: NumpyArray = np.zeros(shape=128);
		for i in range(1, rowCount):
			time: float = (i - 1) / self.BEAT_RESOLUTION;
			deltaState: NumpyArray = (src[i, :] - lastState) != 0;
			for j in range(128):
				if (deltaState[j]):
					if time not in self.states:
						self.states[time] = [];
					self.states[time].append((j, float(src[i, j]) / 255.0))
			lastState = src[i, :];
	def Vectorize(self) -> NumpyArray:
		import numpy as np;
		# Initialize
		times = list(self.states.keys());
		rowCount: int = round(self.BEAT_RESOLUTION * times[-1]) + 1;
		mat: np.ndarray = np.zeros(dtype='float', shape=(rowCount + 2, 128));
		# Add Start and Finish rows
		mat[0, :] = np.ones(shape=(1, 128));
		mat[-1, :] = np.ones(shape=(1, 128));
		# Vectorize States
		lastRow: int = 1;
		for t in times:
			# Get Up-to-Time !
			thisRow: int = round(1 + t * self.BEAT_RESOLUTION);
			for r in range(lastRow, thisRow):
				mat[r + 1] = mat[r];
			# Add the Time
			note: Tuple[int, float];
			for note in self.states[t]:
				# Note in Matrix . Value = Velocity !
				mat[thisRow, note[0]] = note[1];
			# And Done !
			lastRow = thisRow;
		return np.uint8(mat * 255);
