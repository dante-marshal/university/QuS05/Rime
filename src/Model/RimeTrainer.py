from Model.RimeModel import RimeModel, RimeModelParams;
class RimeTrainer:
	pathToModel: str;
	pathToInput: str;
	trainMaxEpoch: int;
	theModel: RimeModel;
	dataMaxBatchSize: int;
	theModelParams: RimeModelParams;
	def __init__(self, pathToModel: str, pathToInput: str, trainMaxEpochs: int, dataMaxBatchSize: int, theModelParams: RimeModelParams):
		self.pathToModel = pathToModel;
		self.pathToInput = pathToInput;
		self.trainMaxEpoch = trainMaxEpochs;
		self.theModelParams = theModelParams;
		self.dataMaxBatchSize = dataMaxBatchSize;
	def Run(self) -> None:
		from Model.Dataset.RimeDataset import RimeDataset;
		# Create the Dataset Handler
		theDataset = RimeDataset(self.pathToInput, self.theModelParams.dataLengthRange, self.dataMaxBatchSize);
		# Create the Model
		self.theModel = RimeModel(self.pathToModel, self.pathToInput, self.theModelParams);
		# Train the Model
		self.theModel.Train(theDataset, self.trainMaxEpoch);
		# Save the Model
		self.theModel.SaveModel(True);
