from typing import Union;
from numpy import ndarray as NumpyArray;
from Model.Dataset.RimeDataset import RimeDataset;

class RimeModelParams:
	memoryDepth: int;
	numLatentNotes: int;
	timeResolution: float;
	dataLengthRange: (int, int);
	NEURON_COUNT_NOTES: int = 128;
	def __init__(self, timeResolution: float, memoryDepth: int, numLatentNotes: int, dataLengthRange: (int, int)):
		self.memoryDepth: int = memoryDepth;
		self.numLatentNotes: int = numLatentNotes;
		self.timeResolution: float = timeResolution;
		self.dataLengthRange: (int, int) = dataLengthRange;
	def SaveToFile(self, filePath: str) -> None:
		from json import dump as DumpJson;
		with open(filePath, 'w') as theFile:
			DumpJson({
				"memoryDepth": self.memoryDepth,
				"numLatentNotes": self.numLatentNotes,
				"timeResolution": self.timeResolution,
				"dataLengthRange": {
					"min": self.dataLengthRange[0],
					"max": self.dataLengthRange[1]
				}
			}, theFile);
	@staticmethod
	def LoadFromFile(filePath: str):
		from json import load as LoadJson;
		with open(filePath) as theFile:
			values = LoadJson(theFile);
		return RimeModelParams(
			timeResolution = values['timeResolution'],
			memoryDepth = values['memoryDepth'],
			numLatentNotes = values['numLatentNotes'],
			dataLengthRange = (values['dataLengthRange']['min'], values['dataLengthRange']['max'])
		);
class RimeModel:
	isTrained: bool;
	pathToModel: str;
	pathToInput: str;
	pathToParams: str;
	finalPaddedLength: int;
	params: RimeModelParams;
	def __init__(self, pathToModel: str, pathToInput: str, modelParams: Union[RimeModelParams, None] = None):
		from os.path import join;
		self.pathToModel = pathToModel;
		self.pathToInput = pathToInput;
		self.pathToParams = join(self.pathToModel, 'Params.json');
		if modelParams == None:
			self.LoadParameters();
		else:
			self.params = modelParams;
			self.Initialize();
	def LoadParameters(self) -> None:
		from tensorflow import keras;
		from os.path import isdir;
		if not isdir(self.pathToModel):
			raise FileNotFoundError(F"{self.pathToModel} is not a directory");
		from os.path import isfile;
		if not isfile(self.pathToParams):
			raise FileNotFoundError(F"{self.pathToParams} does not exist");
		self.kModel = keras.models.load_model(self.pathToModel);
		self.params = RimeModelParams.LoadFromFile(self.pathToParams);
		self.isTrained = True;
	def Initialize(self) -> None:
		finalPaddedLength: int = self.params.dataLengthRange[1] + 4;
		if self.params.memoryDepth != finalPaddedLength:
			raise ValueError(F"The memory depth should be {finalPaddedLength}");
		from tensorflow import keras;
		# Layer : IO
		tInputOfNotes = keras.Input((finalPaddedLength, RimeModelParams.NEURON_COUNT_NOTES))
		# Layer : LSTM
		lNotesLSTM = keras.layers.LSTM(self.params.numLatentNotes, return_state=True)
		# Layer : LSTM States
		sNotesLSTMInit = keras.layers.Input(name='sNotesLSTMInit', shape=self.params.numLatentNotes)
		# Layer : Densors
		lNotesDensor = keras.layers.Dense(RimeModelParams.NEURON_COUNT_NOTES, activation=keras.activations.softmax)
		# Layer : Reshape
		lNotesReshape = keras.layers.Reshape((1, RimeModelParams.NEURON_COUNT_NOTES))
		# The Loop
		tNotesLSTM = sNotesLSTMCell = sNotesLSTMInit
		tOutputOfNotes = []
		for i in range(self.params.memoryDepth):
			print(F"{i + 1: 3d} / {self.params.memoryDepth: 3d}")
			# Notes
			tNotesInput = lNotesReshape(tInputOfNotes[:, i, :])
			tNotesLSTM, _, sNotesLSTMCell = lNotesLSTM(tNotesInput, initial_state=[tNotesLSTM, sNotesLSTMCell])
			tNotesDense = lNotesDensor(tNotesLSTM)
			tNotesDense = lNotesReshape(tNotesDense)
			tOutputOfNotes.append(tNotesDense)
			# tOutputOfNotes = tNotesDense
		# Concat
		lConcat = keras.layers.Concatenate(axis=1)
		tOutputOfNotes = lConcat(tOutputOfNotes)
		# Return the Model
		self.kModel: keras.Model = keras.Model(
			[ tInputOfNotes, sNotesLSTMInit ],
			[ tOutputOfNotes ]
		)
		self.SaveArchitecture();
		self.isTrained = False;
	def SaveModel(self, overwrite: bool = False) -> None:
		if not self.isTrained:
			raise RuntimeError('The network should be trained first before saving the model.');
		from os.path import isfile, isdir;
		if isdir(self.pathToModel):
			if overwrite:
				from shutil import rmtree;
				rmtree(self.pathToModel)
			else:
				raise FileExistsError(F"{self.pathToModel} already exists.");
		elif isfile(self.pathToModel):
			raise FileExistsError(F"{self.pathToModel} is a file, It is required to be a Directory !");
		# The path {self.pathToModel} doesn't exist at this point, So let's create it !
		from os import makedirs;
		makedirs(self.pathToModel);
		print(F"Saving the Model @ '{self.pathToModel}'");
		# First, Save the TensorFlow Model
		self.kModel.save(self.pathToModel, overwrite=overwrite);
		# Now, Save extra Parameters to {self.pathToParams} file
		self.params.SaveToFile(self.pathToParams);
		self.SaveArchitecture();
	def SaveArchitecture(self) -> None:
		from os import makedirs;
		from os.path import isdir, join;
		if not isdir(self.pathToModel):
			makedirs(self.pathToModel);
		from tensorflow.keras.utils import plot_model as PlotModel;
		PlotModel(self.kModel, to_file=join(self.pathToModel, 'Architecture.png'), show_shapes=True, show_layer_names=True)
	def Train(self, dataset: RimeDataset, maxEpoch: int) -> None:
		from tensorflow import device as SelectTfDevice;
		with SelectTfDevice('/cpu:0'):
			from tensorflow.keras import losses as KerasLosses, optimizers as KerasOptimizers;
			self.kModel.compile(
				loss=KerasLosses.categorical_crossentropy,
				optimizer=KerasOptimizers.Adam(),
				metrics=[]
			);
			self.kModel.fit(dataset.Generate(maxEpoch, self.params.numLatentNotes));
			# for epochNumber in range(0, maxEpoch):
			# 	if not dataset.NextBatch():
			# 		print(F"Training : End of Dataset, Training finished in {epochNumber} epochs !")
			# 		break;
			# 	print(F"Training : Epoch {epochNumber} / {maxEpoch}", end=', ');
			# 	dIn = dataset.getBatchInputWithLSTM(self.params.numLatentNotes);
			# 	dOut = dataset.getBatchOutput();
			# 	eLoss = self.kModel.train_on_batch(dIn, dOut);
			# 	print(F"Loss = {eLoss}");
			# 	self.kModel.predict(dIn);
		self.isTrained = True;
	def Emit(self, dataset: RimeDataset) -> (NumpyArray, NumpyArray):
		if not dataset.NextBatch(randomBatch=True):
			raise RuntimeError('The dataset is empty. No input, No emit !');
		inputData = dataset.getBatchInputWithLSTM(self.params.numLatentNotes);
		from tensorflow import device;
		with device('/cpu:0'):
			ret = self.kModel.predict(inputData);
		ret: NumpyArray = ret[0, :, :]
		inputData: NumpyArray = inputData[0][0, :, :];
		ret[ret < ret.mean()] = 0;
		from numpy import concatenate as NpConcatenate;
		from Model.Dataset.RimeMidi import RimeMidi;
		return (inputData, ret)
