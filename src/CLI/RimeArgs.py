import json;
from os import path;
from sys import argv;
from typing import Tuple, Union;
from typing_extensions import Literal;

class RimeArgs:
	command: Literal['-', 'train', 'emit', 'perceptilabs'] = '-';
	needsHelp: bool = False;
	# Path to TF Model [dir]
	pathToModel: str = 'Model';
	# Path to Input Samples [file.midi / dir]
	pathToInput: str = 'Input';
	# Path to Emission Output [file.midi / dir]
	pathToOutput: str = 'Output';
	# Training maximum epochs
	trainMaxEpochs: int = 100;
	# Training batch size
	trainDataBatchSize: int = 64;
	# Data time length range; i.e. The range of the length of data vectors that are fed in or out of the network.
	dataLengthRange: Tuple[int, int] = (32, 64);
	# Training Resolution [frac]
	trainResolution: float = 1.0 / 32.0;
	# Model Memory Depth [#]
	trainMemoryDepth: int = 128;
	# Model Latent Neuron count [#]
	trainNumLatentNotes: int = 128;
	def __init__(self):
		argc = len(argv);
		if argc > 1:
			for i in range(1, argc):
				arg = argv[i];
				(arg, val) = arg.split('=', 1) if '=' in arg else (arg, None);
				self.ParseArg(arg.strip().lower(), None if val == None else val.strip());
	def __str__(self) -> str:
		return json.dumps(self.__dict__);
	def ParseArg(self, arg: str, val: Union[str, None]) -> None:
		if val == None:
			if arg == '?' or arg == '-?' or arg == '-h' or arg == 'help' or arg == '--help':
				self.needsHelp = True;
			else:
				self.ParseCommand(arg);
		else:
			if arg == 'cmd' or arg == 'command':
				self.ParseCommand(val);
			elif arg == 'in' or arg == 'input':
				self.ParseInput(val);
			elif arg == 'out' or arg == 'output':
				self.pathToOutput = val;
			elif arg == 'm' or arg == 'model':
				self.pathToModel = val;
			elif arg == 'epochs' or arg == 'maxepochs':
				self.trainMaxEpochs = int(val);
			elif arg == 'batch' or arg == 'batchsize':
				self.trainDataBatchSize = int(val);
			elif arg == 'res' or arg == 'resolution':
				self.trainResolution = self.ParseFraction(val);
			elif arg == 'depth' or arg == 'memorydepth':
				self.trainMemoryDepth = int(val);
			elif arg == 'len' or arg == 'length':
				self.dataLengthRange = self.ParseIntRange(val);
			elif arg == 'lat' or arg == 'latent':
				self.trainNumLatentNotes = int(val);
			else:
				raise ValueError('Unknown Option "{}"'.format(arg));
	def ParseCommand(self, val: str) -> None:
		val = val.lower();
		if val in ['train', 'emit', 'perceptilabs']:
			self.command = val;
		else:
			raise ValueError('Unknown Command : "{}"'.format(val));
	def ParseInput(self, val: str) -> None:
		if not path.exists(val):
			raise ValueError('Path "{}" does not exist.'.format(val));
		self.pathToInput = val;
	def ParseFraction(self, val: str) -> float:
		from re import compile as CompileRegEx;
		reValidMatch = CompileRegEx(r'(\d+ *\/ *\d+)|(\d+\.\d+)|\d+').fullmatch;
		if reValidMatch(val) == None:
			raise SyntaxError('Invalid Fraction number "{}"'.format(val));
		return eval(val);
	def ParseIntRange(self, val: str) -> (int, int):
		from re import compile as CompileRegEx;
		reValidMatch = CompileRegEx(r'\(\d+ *, *\d+\)').fullmatch(val);
		if reValidMatch != None:
			(retMin, retMax) = reValidMatch.group(0).strip('()').split(',');
			return (int(retMin), int(retMax));
		else:
			reValidMatch = CompileRegEx(r'\d+').fullmatch(val);
		if reValidMatch != None:
			value = int(reValidMatch.group(0));
			return (value, value);
		else:
			raise SyntaxError('Invalid Range "{}"'.format(val));
	def gMsgHelp(self, indent: str = (140 * '-') + '\n') -> str:
		ret: str = '\nRIME : Rime Intelligent Melody Emitter\n';
		ret += indent + self.gMsgUsage();
		ret += indent + self.gMsgOptions();
		ret += indent + self.gMsgExamples();
		return ret + '\n';
	def gMsgUsage(self, indent: str = '') -> str:
		cmd: str = path.basename(argv[0]);
		return indent + 'Usage :\n' + indent + '\t' + cmd + ' [Option=Value] <Command> [Option=Value]\n' + indent + '\t' + cmd + ' [Option=Value] command=<Command> [Option=Value]\n';
	def gMsgOptions(self, indent: str = '') -> str:
		ret = indent + 'Options are explained below including their <Types> and (Default Values)\n';
		ret += indent + 'Options (General) :\n';
		ind = indent + '\t';
		ret += ind + '?, -?, -h, Help, --help   :   < - >                 ( - )                 Show Help Message\n';
		ret += ind + 'cmd, command              :   <"Train", "Emit">     ( - )                 Set the Command\n';
		ret += ind + 'in, input                 :   <Path>                ("Input")             Set the Input Path\n';
		ret += ind + 'm, model                  :   <Path>                ("Model")             Set the path to TF Model directory\n';
		ret += indent + 'Options (Training) :\n';
		ret += ind + 'res, resolution                :   <Fraction>            (1/16)                Override the Time Resolution [in Quarter Note]\n';
		ret += ind + 'epochs, maxEpochs         :   <Integer>             (1000)                Override the maximum number of epochs to train on data\n';
		ret += ind + 'depth, memoryDepth        :   <Integer>             (128)                 Override the Memory Depth\n';
		ret += ind + 'lat, latent               :   <Integer>             (128)                 Override the LSTM Latent Count\n';
		ret += indent + 'Options (Emission) :\n';
		ret += ind + 'out, output               :   <Path>                ("Output")            Set the Output Path\n';
		ret += ind + 'len, length               :   <Seconds>             (30)                  Override the length of emitted melody [s]\n';
		return ret;
	def gMsgExamples(self, indent: str = '') -> str:
		cmd: str = path.basename(argv[0]);
		ret: str = indent + 'Examples :\n';
		indent = indent + '\t';
		ret += indent + cmd + ' Train m="MyModel/Test 01"\n';
		ret += indent + '\tTrains and saves a model in relative path "MyModel/Test 01" using default settings\n';
		ret += indent + cmd + ' Train Model=MyModel res=1/64\n';
		ret += indent + '\tTrains and saves a model in relative path "MyModel" using 1/64th of a Quarter Note as Resolution\n';
		ret += indent + cmd + ' Train m="MyModel/Test 01"\n';
		ret += indent + '\tTrains and saves a model in relative path "MyModel/Test 01" using default settings\n';
		ret += indent + cmd + ' Emit In="Input/Test.mid" output="Output/Test.mid" M=Model\n';
		ret += indent + '\tEmits a melody to relative path "Output/Test.mid" using model previously trained and saved in relative path "Model"\n';
		return ret;
