from CLI.RimeArgs import RimeArgs;
from DevProgram import DevMain;

def Main():
	args = RimeArgs();
	print('DBG : Args = ' + str(args));

	def MainTrain():
		from Model.RimeTrainer import RimeTrainer;
		from Model.RimeModel import RimeModelParams;
		print('RIME Training Session !');
		print(F'\tModel        : {args.pathToModel}');
		print(F'\tInput        : {args.pathToInput}');
		print(F'\tResolution   : {args.trainResolution}');
		print(F'\tData Length  : {args.dataLengthRange}');
		print(F'\tMemory Depth : {args.trainMemoryDepth}');
		print(F'\tLatent Count : {args.trainNumLatentNotes}');
		mParams = RimeModelParams(
			timeResolution = args.trainResolution,
			memoryDepth = args.trainMemoryDepth,
			numLatentNotes = args.trainNumLatentNotes,
			dataLengthRange = args.dataLengthRange
		);
		RimeTrainer(args.pathToModel, args.pathToInput, args.trainMaxEpochs, args.trainDataBatchSize, mParams).Run();

	def MainEmit():
		from Model.RimeEmitter import RimeEmitter
		print('RIME Emission Session !');
		print(F'\tModel  : {args.pathToModel}');
		print(F'\tInput  : {args.pathToInput}');
		print(F'\tOutput : {args.pathToOutput}');
		RimeEmitter(args.pathToModel, args.pathToInput, args.pathToOutput).Run();
	
	def MainPerceptiLabs():
		from Model.Dataset.RimeDataset import RimeDataset;
		print('RIME Preprocessing data for Perceptilabs !');
		print(F'\tInput  : {args.pathToInput}');
		print(F'\tOutput : {args.pathToOutput}');
		theDataset = RimeDataset(args.pathToInput, args.dataLengthRange, args.trainMemoryDepth, args.trainDataBatchSize);
		epochNumber: int = 0;
		from os import makedirs;
		from os.path import join, isdir;
		pIn = join(args.pathToOutput, 'In')
		pOut = join(args.pathToOutput, 'Out')
		if not isdir(pIn):
			makedirs(pIn);
		if not isdir(pOut):
			makedirs(pOut);
		from numpy import savetxt;
		with open(join(args.pathToOutput, 'Data.csv'), 'w') as f:
			f.write('In,Out\n');
			while epochNumber < args.trainMaxEpochs and theDataset.NextBatch():
				dIn = theDataset.getBatchInputWithLSTM();
				dOut = theDataset.getBatchOutput();
				for batchNumber in range(args.trainDataBatchSize):
					pIn = join('In', F'{epochNumber:03d}.{batchNumber:03d}.csv');
					pOut = join('Out', F'{epochNumber:03d}.{batchNumber:03d}.csv');
					savetxt(join(args.pathToOutput, pIn), dIn[batchNumber, :, :], delimiter=',', fmt='%d');
					savetxt(join(args.pathToOutput, pOut), dOut[batchNumber, :, :], delimiter=',', fmt='%d');
					f.write(pIn + ',' + pOut + '\n');
					f.flush();
				epochNumber += 1;
		print('Preprocessing data for Perceptilabs finished')
	if args.needsHelp:
		print(args.gMsgHelp());
	elif args.command == '-':
		print('No command was supplied.')
		print(args.gMsgHelp());
	elif args.command == 'train':
		MainTrain();
	elif args.command == 'emit':
		MainEmit();
	elif args.command == 'perceptilabs':
		MainPerceptiLabs();

DevMain();
Main();
