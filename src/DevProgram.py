import numpy as np;
np.set_printoptions(linewidth=np.inf);
def DevMain():
	return;
	srcDir = 'B:/Proj/University/Sem05/Bachelor Project/Project.RnD/run/Input/Midi';
	from os import listdir;
	srcFiles = listdir(srcDir);
	from Model.Dataset.RimeMidi import RimeMidi;
	srcFiles = srcFiles[1024:1030];
	for fNameMidi in srcFiles:
		from os.path import join;
		fNamePng = F"{fNameMidi[0:-5]}.png";
		fPathMidi = join(srcDir, fNameMidi);
		m = RimeMidi(fPathMidi);
		from os.path import abspath, isdir;
		dPath = abspath('tst');
		from os import makedirs;
		if not isdir(dPath):
			makedirs(dPath);
		m.SaveMatrix(F'tst/{fNamePng}');
		m = RimeMidi(F'tst/{fNamePng}');
		m.SaveMidi(F'tst/{fNameMidi}');
		m = RimeMidi(F'tst/{fNameMidi}');
		m.SaveMatrix(F'tst/{fNamePng}');
	exit(0);
